import React, { useState } from "react";
import {
  Switch,
  Route,
  NavLink,
} from "react-router-dom"

import TodoList from "./components/TodoList"
import {connect, Connect} from 'react-redux'
import { addTodo, handleClearCompletedTodos } from "./actions/todosActions";

function App(props) { 
  
  
  const [inputText, setInputText] = useState("")

  const handleAddTodo = (event)=> {
    if(event.which=== 13){
     props.addTodo(inputText)
      setInputText("")
    }
 }




return (
  <section className="todoapp">
    <header className="header">
      <h1>todos</h1>
      <input 
        onChange={(event)=> setInputText(event.target.value)}
        onKeyDown={(event)=>handleAddTodo(event)}
        className="new-todo"
        value={inputText}
        placeholder="What needs to be done?"
        autoFocus/>
    </header>
    
      <Switch>
        <Route exact path="/">
            <TodoList 
              todos={Object.values(props.todos)}
             
            
              
            />
        </Route>
        <Route exact path="/active">
          <TodoList
            todos={Object.values(props.todos).filter(todo =>!todo.completed
            )}
            
           
            
          />
        </Route>
        <Route  path="/completed"> 
        <TodoList
            todos={Object.values(props.todos).filter(todo =>todo.completed
            )}
            
            
          />
        </Route>
      </Switch>
          <footer className="footer">

        <span className="todo-count">
          <strong> item(s) left
            {Object.values(props.todos).filter(todo =>!todo.completed).length}
          </strong>
        </span>
          <ul className="filters">
          <li>
          <NavLink exact to="/"
          activeClassName="selected">All</NavLink>
          </li>
          <li>
          <NavLink to="/active"
          activeClassName="selected">Active</NavLink>
          </li>
          <li>
          <NavLink to="/completed" 
          activeClassName="selected">
            Completed</NavLink>
        </li>
        </ul>
        <button className="clear-completed"
        onClick={()=> props.handleClearCompleted()}>Clear completed</button>
        </footer>
 
  </section>
);
  
}




const mapStateToProps = (state) =>({
  todos: state.todos
});

const mapDispatchProps = (dispatch) => ({
  addTodo: inputText => dispatch(addTodo(inputText)),
  
   
  handleClearCompleted: () => dispatch(handleClearCompletedTodos())
})
 export default connect(mapStateToProps, mapDispatchProps)(App);

