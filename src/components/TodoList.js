import React  from "react"
import TodoItem  from "./TodoItem"



export default function TodoList(props) {
    
    return (
        <section className="main">
          <ul className="todo-list">
            {props.todos.map((todo) => (
              <TodoItem
               id = {todo.id}
                title={todo.title}
                completed={todo.completed}
                
                
              />
            ))}
          </ul>
        </section>
      );
    
  }