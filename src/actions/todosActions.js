export const  ADDTODO = 'ADDTODO'
export const TOGGLETODO = 'TOGGLETODO'

export const DELETETODO = 'DELETETODO'

export const HANDLECLEARCOMPLETEDTODOS = 'HANDLECLEARCOMPLETEDTODOS'


export const addTodo = (inputText) => ({type: ADDTODO, payload:{inputText: inputText}})

export const toggleTodo = (id) =>({type: TOGGLETODO, payload: {id}})

export const deleteTodo = (id) =>({type: DELETETODO, payload: {id}})

export const handleClearCompletedTodos = () =>({type: HANDLECLEARCOMPLETEDTODOS,})